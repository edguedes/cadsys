import { Component, OnInit } from '@angular/core';
import {Product} from "../product.model";
import {ProductService} from "../product.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-product-read',
  templateUrl: './product-read.component.html',
  styleUrls: ['./product-read.component.css']
})
export class ProductReadComponent implements OnInit {

  productsList: Product[]
  displayedColumns = ['id', 'name', 'price', 'action']
  id: string;

  constructor(
      private productService: ProductService,
      private route: ActivatedRoute,
      private router: Router
  ) { }

  ngOnInit(): void {
    this.listProducts();
    this.id = this.route.snapshot.paramMap.get('id')
  }

  listProducts(): void {
    this.productService.readAll().subscribe(products =>
      this.productsList = products)
  }
}
