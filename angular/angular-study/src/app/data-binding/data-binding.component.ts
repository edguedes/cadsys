import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-binding',
  templateUrl: './data-binding.component.html',
  styleUrls: ['./data-binding.component.css']
})
export class DataBindingComponent implements OnInit {

  url: string = 'https://www.linkedin.com/in/ed-guedes/';
  urlImage = 'http://lorempixel.com/400/200/nature/';
  texto: string = '';

  nomeCurso: string = 'Angular';

  valorInicial = 15;

  constructor() { }

  ngOnInit(): void {
  }

  botaoClicado() {
    alert('Botão clicado !!!')
  }

  onKeyUp(evento: KeyboardEvent) {
    this.texto = (<HTMLInputElement>evento.target).value
  }

}
