import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-output-property',
  templateUrl: './output-property.component.html',
  styleUrls: ['./output-property.component.css']
})
export class OutputPropertyComponent implements OnInit {

  @Input()
  valor: number = 0;

  constructor() { }

  ngOnInit(): void {
  }

  incrementa(): void {
    this.valor++;
  }

  decrementa(): void {
    this.valor--;
  }

}
